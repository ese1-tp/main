from test.testcases import Window1TestCase, Window3TestCase
from core.obfuscate import Obfuscate
from gui.window1 import Window1
from gui.window2 import Window2
from gui.window3 import Window3
from gui.window4 import Window4
import os
import re
import unittest


class Test4(unittest.TestCase):

    def test_req_4_1(self):
        self.window = Window1()
        assert self.window.root.winfo_exists()
        self.window.close()

        self.window = Window2(0)
        assert self.window.root.winfo_exists()
        self.window.close()

        self.window = Window3(Obfuscate)
        assert self.window.root.winfo_exists()
        self.window.close()

        self.window = Window4()
        assert self.window.root.winfo_exists()
        self.window.close()


class Test5(Window1TestCase):

    def test_req_5_1(self):
        b = self.window.get_document_browse_button()
        assert b.winfo_exists()

    def test_req_5_2(self):
        b = self.window.get_keywords_browse_button()
        assert b.winfo_exists()


class Test6(Window1TestCase):

    def test_req_6_1(self):
        label = self.window.get_keywords_label()
        assert label.winfo_exists()

    def test_req_6_2(self):
        label = self.window.get_keywords_label()
        text = label.cget('text')
        if os.sep == '\\':
            regex = r''
        else:
            regex = r'^(.+)\/([^\/]+)$'
        rc = re.compile(regex)
        rc.match(text)

    def test_req_6_3(self):
        Test5.test_req_5_1(self)

    def test_req_6_4(self):
        self.window = Window1()
        pass
        self.window.close()

        self.window = Window1()
        pass


class Test7(Window1TestCase):

    def test_req_7_1(self):
        Test5.test_req_5_2(self)

    def test_req_7_2(self):
        pass


class Test8(Window1TestCase):

    def test_req_8_1(self):
        pass


class Test9(Window1TestCase):

    def test_req_9_1(self):
        pass


class Test10(Window1TestCase):

    def test_req_10_1(self):
        pass

    def test_req_10_2(self):
        pass


class Test11(Window3TestCase):

    def test_req_11_1(self):
        pass


class Test12(Window3TestCase):

    def test_req_12_1(self):
        pass


class Test13(Window3TestCase):

    def test_req_13_1(self):
        pass


class Test14(Window3TestCase):

    def test_req_14_1(self):
        pass

    def test_req_14_2(self):
        pass


if __name__ == "__main__":
    unittest.main()
