from gui.window1 import Window1
from gui.window2 import Window2
from gui.window3 import Window3
from gui.window4 import Window4
import _tkinter as _tk
import unittest


class Window1TestCase(unittest.TestCase):
    """fixtures:
        - Window 1
    """
    def setUp(self):
        self.window = Window1()
        self.root = self.window.get_root()
        self.pump_events()

    def tearDown(self):
        self.window.close()

    def pump_events(self):
        while self.root.dooneevent(_tk.ALL_EVENTS | _tk.DONT_WAIT):
            pass


class Window2TestCase(unittest.TestCase):
    """fixtures:
        - Window 2
    """
    def setUp(self):
        self.window = Window2()
        self.root = self.window.get_root()
        self.pump_events()

    def tearDown(self):
        self.window.close()

    def pump_events(self):
        while self.root.dooneevent(_tk.ALL_EVENTS | _tk.DONT_WAIT):
            pass


class Window3TestCase(unittest.TestCase):
    """fixtures:
        - Window 3
    """
    def setUp(self):
        self.window = Window3()
        self.root = self.window.get_root()
        self.pump_events()

    def tearDown(self):
        self.window.close()

    def pump_events(self):
        while self.root.dooneevent(_tk.ALL_EVENTS | _tk.DONT_WAIT):
            pass


class Window4TestCase(unittest.TestCase):
    """fixtures:
        - Window 4
    """
    def setUp(self):
        self.window = Window4()
        self.root = self.window.get_root()
        self.pump_events()

    def tearDown(self):
        self.window.close()

    def pump_events(self):
        while self.root.dooneevent(_tk.ALL_EVENTS | _tk.DONT_WAIT):
            pass
