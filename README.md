# ESE1 Main

## Contributing

This project uses `pip` and `virtualenv` to manage dependencies in an isolated environment.

Usage of [`virtualenvwrapper`](https://python-guide-cn.readthedocs.io/en/latest/dev/virtualenvs.html#virtualenvwrapper) is recommended for easier management of virtual environments.

Make sure you have Python and Pip installed, and run:

```sh
$ pip install virtualenv
$ virtualenv leonardo
$ source leonardo/bin/activate
$ pip install -r requirements.txt
```

...And let automation do the rest!

- Multiple **git hooks** are set up using the `pre-commit` framework to automatically enforce code quality standards, notably:
  - isort `import` sorter,
  - *Black* formatter,
  - flake8 linter,
  - ... all extended to all `.py` source files and `.ipynb` notebooks in the `prototype` subdirectory!
  - Plus many more: see `.pre-commit-config.yaml` for the complete details.
