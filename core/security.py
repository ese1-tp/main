import re


class Security:
    """The superclass for all Security Tiers."""

    category = "REDACTED"  # blanket term for all security categories

    def __init__(self, subject, line, line_num):
        """Initialise a blank instance.

        Arguments:
            subject: the subject.
            line: the content of the line.
            line_num: the line number.
        """
        self.subject = subject
        self.line = line
        self.line_num = line_num
        # find the index by counting the words that come before the subject
        self.idx = len(line[: line.lower().find(subject.lower())].split())

    def obfuscate(self, line):
        """Pass in the line to obfuscate as it may have been obfuscated before.

        Arguments:
            line: the line to obfuscate.

        Returns:
            The result of the obfuscation.
        """
        return re.sub(self.subject, f"[{self.category.upper()}]", line, flags=re.I)


class Secret(Security):
    """Class for the 'Secret' Security Tier."""

    category = "SECRET"

    def __init__(self, subject, line, line_num):
        """Initialise a blank instance.

        Arguments:
            subject: the subject.
            line: the content of the line.
            line_num: the line number.
        """
        super().__init__(subject, line, line_num)

    def obfuscate(self, line):
        """Refer to the main Obfuscate class for obfuscation.

        Arguments:
            line: the line to obfuscate.

        Returns:
            The result of the obfuscation carried out by the parent Obfuscate class.
        """
        return super().obfuscate(line)


class Restricted(Security):
    """Class for the 'Restricted' Security Tier."""

    category = "RESTRICTED"

    def __init__(self, subject, line, line_num):
        """Initialise a blank instance.

        Arguments:
            subject: the subject.
            line: the content of the line.
            line_num: the line number.
        """
        super().__init__(subject, line, line_num)


class Sensitive(Security):
    """Class for the 'Sensitive' Security Tier."""

    category = "SENSITIVE"

    def __init__(self, subject, line, line_num):
        """Initialise a blank instance.

        Arguments:
            subject: the subject.
            line: the content of the line.
            line_num: the line number.
        """
        super().__init__(subject, line, line_num)
