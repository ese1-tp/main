import os
import random
from pathlib import Path

import spacy
from spacy.training.example import Example

# Import requirements
from spacy.util import compounding, minibatch

from . import spacy_model


class TrainModel:
    """Class encapsulating custom NLP model training."""

    def __init__(self):
        """Initialise a blank instance, and load the standard model."""
        self.iterations = 0
        self.training_data = []
        self.dir_path = (
            os.path.dirname(os.path.realpath(__file__))
            + "\spacy_model\en_spacy_model-1"
        )
        self.nlp = spacy_model.load()

    def get_path(self):
        """Fetch the model path.

        Returns:
            The path of the custom model.
        """
        return self.dir_path

    def add_training_data(self, word, sentence):
        """Add data to train the model.

        Arguments:
            word: the word to add to the training dataset.
            sentence: the sentence to add to the training dataset.
        """
        start = sentence.find(word)
        if start == -1:
            sentence = sentence.lower()
            start = sentence.find(word)
        end = start + len(word)
        self.training_data.append((sentence, {"entities": [(start, end, "CENSORED")]}))

    def get_iterations(self):
        """Fetch the iterations performed while training the model.

        Returns:
            The number of iterations.
        """
        self.iterations = 30
        return self.iterations

    def train(self, window):
        """Train the model on the custom data provided.

        Arguments:
            window: the GUI window requesting the training.
        """
        ner = self.nlp.get_pipe("ner")
        # Resume training
        optimizer = self.nlp.resume_training()
        move_names = list(ner.move_names)
        # List of pipes you want to train
        pipe_exceptions = self.nlp.pipe_names
        # List of pipes which should remain unaffected in training
        other_pipes = [
            pipe for pipe in self.nlp.pipe_names if pipe not in pipe_exceptions
        ]
        # Begin training by disabling other pipeline components
        with self.nlp.disable_pipes(*other_pipes):
            sizes = compounding(1.0, 4.0, 1.001)
            # Training for 30 iterations
            for itn in range(30):
                window.update_progress_bar(itn)
                # shuffle examples before training
                random.shuffle(self.training_data)
                # batch up the examples using spaCy's minibatch
                batches = minibatch(self.training_data, size=sizes)
                # ictionary to store losses
                losses = {}
                for batch in batches:
                    texts, annotations = zip(*batch)
                    example = []
                    # Update the model with iterating each text
                    for i in range(len(texts)):
                        doc = self.nlp.make_doc(texts[i])
                        example.append(Example.from_dict(doc, annotations[i]))
                    # Update the model
                    self.nlp.update(example, drop=0.5, losses=losses)
        window.update_progress_bar(self.iterations)
        window.mainloop()

    def save(self):
        """Write the model contents to storage."""
        self.nlp.to_disk("spacy_model/en_spacy_model-1")
