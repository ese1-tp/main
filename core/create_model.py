# Train NER from a blank spacy model
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
print(dir_path)
from pathlib import Path

import spacy

nlp = spacy.load("en_core_web_md")

ner = nlp.get_pipe("ner")
ner.add_label("CENSORED")
optimizer = nlp.create_optimizer()
output_dir = Path(dir_path + "/spacy_model/en_spacy_model-1/")
nlp.to_disk(output_dir)
