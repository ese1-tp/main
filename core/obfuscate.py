import re
import sys

import spacy
from spacy.matcher import DependencyMatcher

from .security import Restricted, Secret, Sensitive

sys.path.append("./")


class Obfuscate:
    """Obfuscate class.

    Fields:
        instances: instances of obfuscation of class Security
        lines: lines in the document (post-obfuscation)
    """

    def __init__(self):
        """Initialise a blank instance."""
        self.instances = []
        self.lines = []


def get_similar_keyword(token, nlp_keywords, detected, tracker, level=0):
    """Link a token detected to be obfuscated to a keyword based on a
    similarity level.

    Arguments:
        token: spacy token to be linked
        nlp_keywords: dictionary of keyword tokens with values of its risk type
        detected: dictionary used to store previously "linked" tokens
            detected[token]["keyword"]: stores spacy token of keyword linked with token
            detected[token]["sim"]: stores similarity level of keyword and token
        tracker: dictionary to keep track of each tokens linked with a keyword
        level: the similarity level threshold.

    Returns:
        detected: dictionary with additional token being stored in it
        tracker: dictionary with tokens being stored in its respective linked keyword
    """
    similarity = 0
    k = None
    for keyword in nlp_keywords.keys():
        if keyword.similarity(token) > similarity and keyword.similarity(token) > level:
            similarity = keyword.similarity(token)
            k = keyword
    if k is not None:
        tracker[k].append([token, similarity])
        if token.text.lower() not in detected.keys():
            detected[token.text.lower()] = {"keyword": nlp_keywords[k]}
            detected[token.text.lower()]["sim"] = similarity
            report_similarity(token.text.lower(), k.text.lower(), nlp_keywords[k])
    return detected, tracker


def setup_matcher(nlp):
    """Part of Speech and Dependency detection parser.

    Defines all the patterns used to find common POS and dependency links based on a synthetic sample.

    Arguments:
        nlp: spacy nlp model used

    Returns:
        dep_matcher: matcher used to find instances that fits pattern defined below
    """
    dep_matcher = DependencyMatcher(vocab=nlp.vocab)
    n_n_compound = [
        {"RIGHT_ID": "noun", "RIGHT_ATTRS": {"POS": "NOUN"}},
        {
            "LEFT_ID": "noun",
            "REL_OP": ">",
            "RIGHT_ID": "subject",
            "RIGHT_ATTRS": {"DEP": "compound"},
        },
    ]
    n_amod = [
        {"RIGHT_ID": "noun", "RIGHT_ATTRS": {"POS": "NOUN"}},
        {
            "LEFT_ID": "noun",
            "REL_OP": ">",
            "RIGHT_ID": "subject",
            "RIGHT_ATTRS": {"DEP": "amod"},
        },
    ]
    v_n_npadvmod = [
        {"RIGHT_ID": "root_verb", "RIGHT_ATTRS": {"POS": "VERB"}},
        {
            "LEFT_ID": "root_verb",
            "REL_OP": ">",
            "RIGHT_ID": "noun",
            "RIGHT_ATTRS": {"DEP": "npadvmod"},
        },
    ]
    v_A_a_n_advmod = [
        {"RIGHT_ID": "root_verb", "RIGHT_ATTRS": {"POS": "VERB"}},
        {
            "LEFT_ID": "root_verb",
            "REL_OP": ">",
            "RIGHT_ID": "adv",
            "RIGHT_ATTRS": {"DEP": "advmod", "POS": "ADV"},
        },
        {
            "LEFT_ID": "root_verb",
            "REL_OP": ">",
            "RIGHT_ID": "noun",
            "RIGHT_ATTRS": {"DEP": "dobj", "POS": "NOUN"},
        },
        {
            "LEFT_ID": "noun",
            "REL_OP": ">",
            "RIGHT_ID": "adj",
            "RIGHT_ATTRS": {"DEP": "amod", "POS": "ADJ"},
        },
    ]
    nummod = [
        {"RIGHT_ID": "noun", "RIGHT_ATTRS": {"POS": "NOUN"}},
        {
            "LEFT_ID": "noun",
            "REL_OP": ">",
            "RIGHT_ID": "num",
            "RIGHT_ATTRS": {"DEP": "nummod", "POS": "NUM"},
        },
    ]
    n_a_n_preppobj = [
        {"RIGHT_ID": "noun", "RIGHT_ATTRS": {"POS": "NOUN"}},
        {
            "LEFT_ID": "noun",
            "REL_OP": ">",
            "RIGHT_ID": "adp",
            "RIGHT_ATTRS": {"DEP": "prep", "POS": "ADP"},
        },
        {
            "LEFT_ID": "adp",
            "REL_OP": ">",
            "RIGHT_ID": "final_n",
            "RIGHT_ATTRS": {"DEP": "pobj", "POS": "NOUN"},
        },
    ]
    n_v_acl = [
        {"RIGHT_ID": "noun", "RIGHT_ATTRS": {"POS": "NOUN"}},
        {
            "LEFT_ID": "noun",
            "REL_OP": ">",
            "RIGHT_ID": "verb",
            "RIGHT_ATTRS": {"POS": "VERB", "DEP": "acl"},
        },
    ]

    dep_matcher.add("n_n_compound", patterns=[n_n_compound])
    dep_matcher.add("n_amod", patterns=[n_amod])
    dep_matcher.add("v_n_npadvmod", patterns=[v_n_npadvmod])
    dep_matcher.add("v_A_a_n_advmod", patterns=[v_A_a_n_advmod])
    dep_matcher.add("nummod", patterns=[nummod])
    dep_matcher.add("n_a_n_preppobj", patterns=[n_a_n_preppobj])
    dep_matcher.add("n_v_acl", patterns=[n_v_acl])
    return dep_matcher


def detect_POS(doc, dep_matcher, nlp_keywords, detected, tracker, level=0.45):
    """Detect POS patterns to be obfuscated if similarity level is high enough.

    Arguments:
        doc: spacy nlp model parsed document variable
        dep_matcher: dependency matcher from setup_matcher
        nlp_keywords: dictionary of keywords with its type of risk
        detected: dictionary to store additional tokens
        tracker: dictionary of keywords with tokens linked with it
        level: sensitivity of similarity level between tokens

    Returns:
        detected: dictionary with additional token being stored in it
        tracker: dictionary with tokens being stored in its respective linked keyword
    """
    dep_matches = dep_matcher(doc)

    found = []
    found_ = []
    for match in dep_matches:
        matches = match[1]
        verb, subject = matches[0], matches[1]
        if subject > verb:
            subject, verb = verb, subject
        if (
            doc[subject].text == "o"
            or "\n" in doc[subject : verb + 1].text
            or "Figure" in doc[subject : verb + 1].text
            or "page" in doc[subject : verb + 1].text
            or "(" in doc[subject : verb + 1].text
        ):
            continue

        span = doc[subject : verb + 1]
        if span.text not in found_:
            found_.append(span.text)
            found.append(span)
    found = spacy.util.filter_spans(found)
    for ent in found:
        detected, tracker = get_similar_keyword(
            ent, nlp_keywords, detected, tracker, level
        )

    return detected, tracker


def normalise_words(detected, tracker):
    """Compare the value of each keywords from tracker to get an weighted value
    of similarity.

    Arguments:
        detected: dictionary used to store previously "linked" tokens
            detected[token]["keyword"]: stores spacy token of keyword linked with token
            detected[token]["sim"]: stores similarity level of keyword and token
        tracker: dictionary to keep track of each tokens linked with a keyword

    Returns:
        normalised: dictionary with detected words to be obfuscated that are similar enough to be obfuscated
    """
    normalised = {}
    return_d = {}
    for key, value in tracker.items():
        if len(value) <= 1:
            continue
        for index1, v1 in enumerate(value):
            ent = v1[0]
            avg = 0
            weight = 0
            for index2, v2 in enumerate(value):
                compare = v2[0]
                if index1 == index2:
                    continue
                load = v1[1]
                weight += load
                avg += compare.similarity(ent) * load
            avg /= weight
            if avg > 0.5:
                normalised[ent.text.lower()] = detected[ent.text.lower()]
    temp_norm = [keys for keys in normalised.keys()]
    for keys, value in detected.items():
        if keys in temp_norm and detected[keys]["sim"] > 0.7:
            return_d[keys] = value

    return normalised


def obfuscate_file(nlp, window, obfuscation, nlp_keywords, doc):
    """Detect words to be obfuscated.

    Arguments:
        nlp: spacy nlp model used
        window: current window of the application
        obfuscation: instance of Obfuscate class
        nlp_keywords: dictionary of keyword tokens with values of its risk type
        doc: document after being parsed in the spacy nlp model

    Returns:
        obfuscation: containing new lines where some are already obfuscated
    """
    # opens the project file and store each line in contents
    # * entity detection from presentation notebook
    detected = {}
    tracker = {key: [] for key, value in nlp_keywords.items()}
    for ent in doc.ents:
        #      remove all cardinal/useless numbers
        if ent.label != 397:
            detected, tracker = get_similar_keyword(
                ent, nlp_keywords, detected, tracker
            )

    dep_matcher = setup_matcher(nlp)
    detected, tracker = detect_POS(doc, dep_matcher, nlp_keywords, detected, tracker)
    detected = normalise_words(detected, tracker)

    for i, line in enumerate(obfuscation.lines):
        window.update_progress_bar(i)
        found_project = False

        for j in detected.keys():

            found_project = re.search(f"^.*{j}.*$", line.lower())

            if found_project:
                if detected[j]["keyword"].lower() == "restricted":
                    risk = Restricted(j, line, i)
                elif detected[j]["keyword"].lower() == "secret":
                    risk = Secret(j, line, i)
                elif detected[j]["keyword"].lower() == "sensitive":
                    risk = Sensitive(j, line, i)
                obfuscation.instances.append(risk)
                obfuscation.lines[i] = risk.obfuscate(obfuscation.lines[i])
    window.update_progress_bar(window.input_file_lines)
    window.mainloop()
    return obfuscation


def redact(redact_list, nlp_keywords, nlp):
    """Return new nlp keywords list by using the new keywords from redact list.

    Arguments:
        redact_list: the list of new security keywords.
        nlp_keywords: the list of current nlp keywords.
        nlp: the nlp model.

    Returns:
        new_nlp_keyword_token: the list of new nlp keywords.
    """
    new_nlp_keyword_token = nlp_keywords
    for redact in redact_list:
        keyword_nlp = nlp(redact[0])  # passes in word
        for token in keyword_nlp:
            new_nlp_keyword_token[token] = redact[
                1
            ]  # adding type of word to dictionary

    return new_nlp_keyword_token


def precise_obfuscate(obs, word_lists):
    """Obfuscates exact words in the input file.

    Creates a new Security instance by taking in a sensitive word and the words adjacent to it and uses the new instance to obfuscate the input file.

    Arguments:
        obs: instance of Obfuscate
        word_lists: list of lists with the format [[index_of_sensitive_word, security_level, [<word>,<word>,<word>]],...]
    """
    for lst in word_lists:
        # Combine the words into a single string
        sentence_fragment = " ".join(lst[2])
        # Search for the line containing the sentence fragment
        for i, line in enumerate(obs.lines):
            found_line = re.search(f"^.*{sentence_fragment}.*$", line)
            # Create a Security instance for the sensitive word, add it to the list of instances, and obfuscate the line
            if found_line:
                if lst[1].lower() == "secret":
                    risk = Secret(lst[2][lst[0]], line, i)
                elif lst[1].lower() == "restricted":
                    risk = Restricted(lst[2][lst[0]], line, i)
                elif lst[1].lower() == "sensitive":
                    risk = Sensitive(lst[2][lst[0]], line, i)
                obs.instances.append(risk)
                obs.lines[i] = risk.obfuscate(obs.lines[i])


# obfuscate file sub function to find keywords, should be return a dictionary


def read_keyword(keyword_file, nlp):
    """Open a csv (comma seperated values) file and read in keywords as tokens
    of the used nlp model.

    Arguments:
        keyword_file: path to csv file
        nlp: spacy nlp model used

    Returns:
        nlp_keyword_token: dictionary containing the keywords from the file as keys in the form of a token, value is the type of risk
    """
    nlp_keyword_token = {}
    with open(keyword_file, "r") as f:
        for line in f:
            line_list = line.strip().split(",")
            keyword_nlp = nlp(line_list[0])
            for token in keyword_nlp:
                nlp_keyword_token[token] = line_list[1]

    return nlp_keyword_token


# obfuscate file sub function to read through spec file and put lines into
# a list of lines, should return nlp(doc)
# refer to presentationt to what nlp(doc) is
def read_spec_file(obs, spec_file, nlp):
    """Open a text file and read aech line into an instance of the Obfuscate
    class.

    Arguments:
        obs: instance of obfuscate class to store each line of the file in
        spec_file: path to text file (*.txt)
        nlp: spacy nlp model used

    Returns:
        obs: instance of obfuscate class with each line of the file stored in
        doc: parsed document using the spacy nlp model
    """
    with open(spec_file, "r") as f:
        for index, line in enumerate(f):
            obs.lines.append(line)
    with open(spec_file, "r") as f:
        text = f.read()
    doc = nlp(text)
    return obs, doc


def initialise_report(result):
    """Initialise the report file for use in rest of program.

    Arguments:
        result: the result of nlp processing.

    Returns:
        report_file: the name of the report file.
    """
    global report_file
    report_file = result[:-1]
    report_file.append("report.txt")
    report_file = "/".join(report_file)
    open(report_file, "w").close
    return report_file


def report_similarity(token, k, risk):
    """Write the security tier of an instance to the report file.

    Arguments:
        token: the subject token from the original document.
        k: the specific keyword from the keywords file.
        risk: the security tier of the keyword.
    """
    with open(report_file, "a") as report:
        report.write(
            "'"
            + token
            + "'  has been redacted as it has been classified as '"
            + risk
            + "' because there is similarity detected between goldfnger at keyword '"
            + k
            + "' from keywords file\n\n"
        )
