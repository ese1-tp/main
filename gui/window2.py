import sys
from math import floor
from pathlib import Path
from tkinter import HORIZONTAL, Label, PhotoImage, Tk, ttk


class Window2:
    """The class representing the first window of the GUI.

    This window is mostly used to show progress through the obfuscation
    process.
    """

    def __init__(self, lines):
        """Intialise window2 with the widgets.

        Arguments:
            lines: the number of total lines in the input document.
        """
        self.progress1 = "censored "
        self.progress2 = " Lines"
        self.progress3 = "Obfuscation | Training in progress . . ."
        self.input_file_lines = lines
        self.root = Tk()
        self.root.geometry("550x100")

    def start(self):
        """Set up the window with the correct details about the process."""
        self.root.title(self.progress3)
        if getattr(sys, "frozen", False):
            folder = Path(sys._MEIPASS)
        else:
            folder = Path(__file__).parent
        self.root.tk.call(
            "wm", "iconphoto", self.root._w, PhotoImage(file=folder / "icon.png")
        )
        self.progress = ttk.Progressbar(
            self.root, orient=HORIZONTAL, length=500, mode="determinate"
        )
        self.progress_label = Label(
            self.root,
            text=self.progress1 + "0 / " + str(self.input_file_lines) + self.progress2,
        )
        self.progress_label.grid(column=1, row=1)
        self.progress.grid(column=1, row=2)

    def mainloop(self):
        """Open window2."""
        self.root.mainloop()

    def set_to_training(self):
        """Change the configuration of the window to work with the training
        process."""
        self.progress1 = "training "
        self.progress2 = " Iterations"
        self.progress3 = "Obfuscation | Training in progress . . ."

    def close(self):
        """Destroy window2."""
        self.root.destroy()

    def update_progress_bar(self, line):
        """Update the progress bar.

        Arguments:
            line: the index of the line currently being processed.
        """
        self.progress_label.configure(
            text=self.progress1
            + str(line)
            + " / "
            + str(self.input_file_lines)
            + self.progress2
        )
        self.progress["value"] = floor((line / self.input_file_lines) * 100)
        self.root.update_idletasks()
        if line == self.input_file_lines:
            self.close()
