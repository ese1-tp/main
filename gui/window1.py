import sys
from pathlib import Path
from tkinter import (
    Button,
    Entry,
    Label,
    PhotoImage,
    StringVar,
    Tk,
    filedialog,
    messagebox,
)


class Window1:
    """The class representing the first window of the GUI.

    This window gathers user input such as:
    - input file path
    - keywords file path
    - output file name
    """

    def __init__(self):
        """Intialise window1 with the widgets."""
        self.keywords_file = ""
        self.output_name = ""
        self.input_file = ""
        self.root = Tk()
        self.output_name_var = StringVar()
        self.root.geometry("600x200")
        if getattr(sys, "frozen", False):
            folder = Path(sys._MEIPASS)
        else:
            folder = Path(__file__).parent
        photo = PhotoImage(file=folder / "icon.png")
        self.root.iconphoto(False, photo)
        self.root.title("Obfuscation | Select File . . .")
        self.label_file_explorer = Label(
            self.root, text="Select the file that will be obfuscated."
        )
        self.label_keywords_file = Label(self.root, text="keywords File: keywords.txt")
        self.button_explore = Button(
            self.root, text="Browse", command=self.browse_files
        )
        self.file_name_label = Label(
            self.root, text="Enter the file name for the obfuscated file:"
        )
        self.sub_btn = Button(self.root, text="Submit", command=self.submit)
        self.chg_btn = Button(
            self.root, text="Change Keywords File . . .", command=self.change
        )
        self.closed = True
        self.next_btn = Button(self.root, text="next", command=self.next)
        self.file_name_entry = Entry(self.root, textvariable=self.output_name_var)
        self.label_file_explorer.place(relx=0.2, rely=0.05, anchor="center")
        self.button_explore.place(relx=0.55, rely=0.05, anchor="center")
        self.file_name_label.place(relx=0.22, rely=0.25, anchor="center")
        self.file_name_entry.place(relx=0.55, rely=0.25, anchor="center")
        self.sub_btn.place(relx=0.75, rely=0.25, anchor="center")
        self.chg_btn.place(relx=0.55, rely=0.45, anchor="center")
        self.label_keywords_file.place(relx=0.155, rely=0.45, anchor="center")
        self.next_btn.place(relx=0.5, rely=0.75, anchor="center")

    def get_input_file(self):
        """Fetch input file path.

        Returns:
            The path of the input document.
        """
        return self.input_file

    def get_closed(self):
        """Fetch closed variable, to check if the window was closed.

        Returns:
            True if the window was closed; False otherwise.
        """
        return self.closed

    def get_keywords_file(self):
        """Fetch keywords file path.

        Returns:
            The path of the keywords file.
        """
        return self.keywords_file

    def get_output_name(self):
        """Fetch output file name.

        Returns:
            The name of the output file.
        """
        return self.output_name

    def next(self):
        """Check if all entries are submitted then calls close()."""
        if self.input_file == "":
            self.pop_up("No file selected to be obfuscated.")
            return
        if self.output_name == "":
            self.pop_up("No output file name entered.")
            return
        self.close()

    def mainloop(self):
        """Activate window1."""
        self.root.mainloop()

    def change(self):
        """Show the selected path for the keywords file in the label and
        intializes the path for the keywords file."""
        file_name = filedialog.askopenfilename(
            initialdir="/",
            title="Select a File",
            filetypes=(("Text files", "*.txt*"), ("all files", "*.*")),
        )
        self.label_keywords_file.configure(
            text="keywords File: " + file_name.split("/")[-1], fg="red", bg="white"
        )
        self.keywords_file = file_name

    def submit(self):
        """Fetch the output name from the entry widget."""
        self.output_name = self.output_name_var.get()

    def browse_files(self):
        """Show the selected path for the input file in the label and
        intializes the path for the input file."""
        file_name = filedialog.askopenfilename(
            initialdir="/",
            title="Select a File",
            filetypes=(("Text files", "*.txt*"), ("all files", "*.*")),
        )
        self.label_file_explorer.configure(
            text="File selected: " + file_name.split("/")[-1], fg="red", bg="white"
        )
        self.input_file = file_name

    def close(self):
        """Destroy window1."""
        self.closed = False
        self.root.destroy()

    def pop_up(self, text):
        """Show a pop-up error message.

        Arguments:
            text: the error text to display in the pop-up message.
        """
        messagebox.showinfo("Obfuscation | Error", text, parent=self.root)
