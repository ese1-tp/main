import sys
from pathlib import Path
from tkinter import (
    END,
    LEFT,
    Button,
    Checkbutton,
    Entry,
    Label,
    OptionMenu,
    PhotoImage,
    StringVar,
    Tk,
    _setit,
    messagebox,
)
from tkinter.scrolledtext import ScrolledText


class Window3:
    """The class representing the third window of the GUI.

    This window enables the user to correct the result of the initial
    obfuscation.
    """

    def __init__(self, obfuscate, input_file):
        """Intialise window3 with the widgets.

        Arguments:
            obfuscate: an instance of the Obfuscate class encapsulating the NLP model.
            input_file: the path of the input file.
        """
        self.input_file = input_file
        self.censored_words = []
        self.clicked3 = ""
        self.clicked2 = ""
        self.word_var = ""
        self.refactor_list1 = []
        self.refactor_list2 = []
        self.redact_word = ""
        self.redact_list = []
        self.back = ""
        self.root = Tk()
        self.redact_word = StringVar(self.root)
        self.clicked = StringVar(self.root)
        self.clicked.set("Secret")
        self.root.geometry("400x150")
        options = ["Secret", "Restricted", "Sensitive"]
        self.clicked.set("Secret")
        drop = OptionMenu(self.root, self.clicked, *options)
        self.root.title("Obfuscation | File Obsufucation Successful . . .")
        if getattr(sys, "frozen", False):
            folder = Path(sys._MEIPASS)
        else:
            folder = Path(__file__).parent
        self.root.tk.call(
            "wm", "iconphoto", self.root._w, PhotoImage(file=folder / "icon.png")
        )
        show = Button(
            self.root,
            text="show removed objects . . .",
            command=lambda: self.checkbox_open(obfuscate),
        )
        label = Label(self.root, text="Enter word . . .")
        entry = Entry(self.root, textvariable=self.redact_word)
        redact = Button(self.root, text="Redact", command=self.redact)
        next_btn = Button(self.root, text="Next", command=self.close)
        back_btn = Button(self.root, text="Back", command=self.set_back_true)
        remove = Button(
            self.root, text="Censor Words Manually", command=self.remove_word
        )
        show.place(relx=0.5, rely=0.1, anchor="center")
        label.place(relx=0.1, rely=0.4, anchor="center")
        entry.place(relx=0.4, rely=0.4, anchor="center")
        drop.place(relx=0.7, rely=0.4, anchor="center")
        redact.place(relx=0.9, rely=0.4, anchor="center")
        remove.place(relx=0.5, rely=0.6, anchor="center")
        back_btn.place(relx=0.25, rely=0.8, anchor="center")
        next_btn.place(relx=0.75, rely=0.8, anchor="center")

    def remove_word(self):
        """Create a window that allows the user to censor specific words."""
        root = Tk()
        self.word_var = StringVar(root)
        self.clicked3 = StringVar(root)
        self.clicked2 = StringVar(root)
        root.title("Obfuscation | Censoring words manually . . .")

        root.geometry("600x100")
        label = Label(root, text="Enter the Sentence:")
        label2 = Label(
            root, text="Choose the word to be censored and the type censorship:"
        )
        word = Entry(root, textvariable=self.word_var)
        censor = Button(root, text="Censor", command=self.append_censored_words)
        options = ["Secret", "Restricted", "Sensitive"]
        list1 = ["empty"]
        self.clicked2.set("Secret")
        drop = OptionMenu(root, self.clicked2, *options)
        drop2 = OptionMenu(root, self.clicked3, *list1)
        submit = Button(
            root, text="Submit", command=lambda: self.submit_sentence(drop2)
        )

        done = Button(root, text="Done", command=root.destroy)
        label.grid(column=1, row=1)
        word.grid(column=2, row=1)
        submit.grid(column=3, row=1)
        label2.grid(column=1, row=2)
        drop2.grid(column=2, row=2)
        drop.grid(column=3, row=2)
        censor.grid(column=4, row=2)
        done.grid(column=2, row=3)
        root.mainloop()

    def submit_sentence(self, drop):
        """Submit the sentence and type of censorship.

        Arguments:
            drop: the drop-down list of security tiers.
        """
        words = self.word_var.get()
        if words == "":
            self.pop_up("No words entered.")
            return
        words = words.split(" ")
        # Reset var and delete all old options
        self.clicked3.set("")
        drop["menu"].delete(0, "end")
        # Insert list of new options (tk._setit hooks them up to var)
        for choice in words:
            drop["menu"].add_command(
                label=choice, command=_setit(self.clicked3, choice)
            )

    def append_censored_words(self):
        """Update the censored_words list that will be used to censor the
        specific words selected."""
        sentence = self.word_var.get().split(" ")
        self.censored_words.append(
            [sentence.index(self.clicked3.get()), self.clicked2.get(), sentence]
        )

    def close(self):
        """Destroy window3."""
        self.back = False
        self.root.destroy()

    def checkbox_close(self):
        """Destroy window3 checkbox list."""
        for i in self.refactor_list1:
            if i not in self.refactor_list2:
                self.refactor_list2.append(i)
        self.refactor_list1 = []
        self.checkbox.destroy()

    def append_refactor_list(self, i):
        """Check whether a checkbutton was pressed; if not, it is added to the
        list.

        Arguments:
            i: the index of the checkbutton.
        """
        if i in self.refactor_list1:
            self.refactor_list1.remove(i)
        else:
            self.refactor_list1.append(i)

    def checkbox_open(self, obfuscate):
        """Intialise window3 checkbox list with widgets and opens it.

        Arguments:
            obfuscate: an instance of the Obfuscate class encapsulating the NLP model.
        """
        self.checkbox = Tk()
        self.output = Tk()
        textpart2 = ScrolledText(self.output, width=400, height=100, wrap="word")

        text = ""
        words = []

        Button(self.checkbox, text="Done", command=self.checkbox_close).pack()
        textpart = ScrolledText(self.checkbox, width=400, height=100)

        textpart.pack()
        self.checkbox.geometry("550x300")
        self.output.geometry("550x300")
        self.output.title("Obfuscation | Input file . . .")
        self.checkbox.title("Obfuscation | File Obsufucation Successful . . .")
        with open(self.input_file) as file1:
            data = file1.read()
            for i in data:
                text += i
        textpart2.insert(1.0, text)
        textpart2.pack(side=LEFT)
        for i, elt in enumerate(obfuscate.instances):
            words.append(elt.subject)
            cb = Checkbutton(
                textpart,
                text=f"{elt.subject} - {elt.line}",
                command=lambda i=i: self.append_refactor_list(i),
                bg="white",
                anchor="w",
            )
            textpart.window_create("end", window=cb)

            textpart.insert("end", "\n")

        for i in words:
            idx = "1.0"
            while idx:
                idx = textpart2.search(i, idx, nocase=1, stopindex=END)
                if idx:
                    lastidx = "%s+%dc" % (idx, len(i))
                    textpart2.tag_add(i, idx, lastidx)
                    idx = lastidx
            textpart2.tag_config(i, background="yellow")

        self.output.mainloop()
        self.checkbox.mainloop()

    def mainloop(self):
        """Open window3."""
        self.root.mainloop()

    def redact(self):
        """Update the redact_list that will be used to add new keywords to re-
        censor the file."""
        self.redact_list.append([self.redact_word.get(), self.clicked.get()])

    def set_back_true(self):
        """Set back to true to indicate that the user wants to go to the
        previous window (window1)."""
        self.back = True
        self.root.destroy()

    def get_back(self):
        """Fetch back to check if the user intends to go to the previous window
        (window1).

        Returns:
            True if the requested window is Window 1; False otherwise.
        """
        return self.back

    def get_redact_word(self):
        """Fetch the new keywords list that will be used to re-censor the file.

        Returns:
            The list of new keywords for re-obfuscation.
        """
        return self.redact_list

    def get_censored_words(self):
        """Fetch the list of specific literals that will be censored.

        Returns:
            The list of new specific literals for re-obfuscation.
        """
        return self.censored_words

    def get_refactor_list(self):
        """Fetch refactor_list2 that contains words that will be uncensored.

        Returns:
            The list of words to be uncensored.
        """
        return self.refactor_list2

    def pop_up(self, text):
        """Show a pop-up error message.

        Arguments:
            text: the error text to display in the pop-up message.
        """
        messagebox.showinfo("Obfuscation | Error", text, parent=self.root)
