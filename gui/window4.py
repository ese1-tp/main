import os
import subprocess
import sys
from pathlib import Path
from tkinter import Button, Checkbutton, IntVar, PhotoImage, Tk


class Window4:
    """The class representing the last window of the GUI.

    This window concludes the process and presents the final output to
    the user.
    """

    def __init__(self, output, keywords_file, report_file):
        """Intialise window4 with the widgets.

        Arguments:
            output: the name of the output file.
            keywords_file: the name of the keywords file.
            report_file: the name of the report file.
        """
        self.output_file = output
        self.keywords_file = keywords_file
        self.report_file = report_file
        self.root = Tk()
        self.root.geometry("400x200")
        self.root.title("Obfuscation | choose files to open . . .")
        if getattr(sys, "frozen", False):
            folder = Path(sys._MEIPASS)
        else:
            folder = Path(__file__).parent
        self.root.tk.call(
            "wm", "iconphoto", self.root._w, PhotoImage(file=folder / "icon.png")
        )
        self.Checkbutton1 = IntVar()
        self.Checkbutton2 = IntVar()
        self.Checkbutton3 = IntVar()

        Button1 = Checkbutton(
            self.root,
            text="Censored document",
            variable=self.Checkbutton1,
            onvalue=1,
            offvalue=0,
            height=2,
            width=20,
        )

        Button2 = Checkbutton(
            self.root,
            text="Report",
            variable=self.Checkbutton2,
            onvalue=1,
            offvalue=0,
            height=2,
            width=20,
        )
        if keywords_file != "":
            Button3 = Checkbutton(
                self.root,
                text="New keywords",
                variable=self.Checkbutton3,
                onvalue=1,
                offvalue=0,
                height=2,
                width=20,
            )
            Button3.place(relx=0.16, rely=0.5, anchor="center")
        next_btn = Button(self.root, text="Finish", command=self.finish)
        Button1.place(relx=0.2, rely=0.1, anchor="center")
        Button2.place(relx=0.11, rely=0.3, anchor="center")

        next_btn.place(relx=0.5, rely=0.7, anchor="center")

    def close(self):
        """Destroy window4."""
        self.root.destroy()

    def finish(self):
        """Open files corrosponding to the selected checkboxes in window4."""
        if self.Checkbutton1.get() == 1:
            self.open_file(self.output_file)
        if self.Checkbutton2.get() == 1:
            self.open_file(self.report_file)
        if self.Checkbutton3.get() == 1:
            self.open_file(self.keywords_file)
        self.close()

    def mainloop(self):
        """Open window4."""
        self.root.mainloop()

    def open_file(self, filename):
        """Open a file using platform-specific system calls.

        This acts as a platform-agnostic interface wrapping platform-specific file openers; includes platform detection logic.

        Arguments:
            filename: the name of the file to open.
        """
        if sys.platform == "win32":
            os.startfile(filename)
        else:
            opener = "xdg-open"
            subprocess.call([opener, filename])
