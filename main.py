"""Manages the interactions between front-end GUI and back-end NLP models.

(Controller in a MVC-like architecture)
"""

import sys
import warnings

# import spacy_model
from core import spacy_model
from core.obfuscate import (
    Obfuscate,
    initialise_report,
    obfuscate_file,
    precise_obfuscate,
    read_keyword,
    read_spec_file,
    redact,
)
from core.train import TrainModel
from gui.window1 import Window1
from gui.window2 import Window2
from gui.window3 import Window3
from gui.window4 import Window4

warnings.simplefilter("ignore", UserWarning)

sys.path.append("./")
model = TrainModel()
nlp = spacy_model.load()
obs = Obfuscate()
retreat = False
jump = False
new_keywords = ""
while 1:
    if not retreat:
        if not jump:
            current_window = Window1()
        current_window.mainloop()
        file_dir = current_window.get_input_file()
        keywords_dir = current_window.get_keywords_file()
        if keywords_dir == "":
            keywords_dir = file_dir.split("/")
            keywords_dir[len(keywords_dir) - 1] = "keywords.txt"
            keywords_dir = "/".join(keywords_dir)
            keywords_dir = str(keywords_dir)
        output_file_name = current_window.get_output_name()
        if current_window.get_closed():
            break
        total = sum(1 for line in open(file_dir))
        result = file_dir.split("/")
        report_file = initialise_report(result)
        try:
            nlp_keywords = read_keyword(keywords_dir, nlp)
        except (FileNotFoundError):
            jump = True
            current_window = Window1()
            current_window.pop_up(
                "Keywords.txt file not found, please browse the file again."
            )
            continue
        obs, doc = read_spec_file(obs, file_dir, nlp)
        current_window = Window2(total)
        current_window.start()
        obs = obfuscate_file(nlp, current_window, obs, nlp_keywords, doc)
        retreat = True
    if retreat:
        retreat = False
        current_window = Window3(obs, file_dir)
        current_window.mainloop()
        pressed_back = current_window.get_back()
        redact_list = current_window.get_redact_word()
        refactor_list = current_window.get_refactor_list()
        if pressed_back == "":
            break
        elif pressed_back:
            continue
        if len(refactor_list) != 0:
            for i in refactor_list:
                subject = obs.instances[i].subject
                obs.lines[obs.instances[i].line_num] = obs.instances[i].line
                obs.instances.remove(obs.instances[i])
                for elt, i in enumerate(obs.instances):
                    if i.subject == subject:
                        obs.lines[obs.instances[elt].line_num] = obs.instances[elt].line
                        obs.instances.remove(obs.instances[elt])

            retreat = True
        if len(redact_list) != 0:
            file = open(keywords_dir, "r")
            keywords = file.read()
            file.close()
            new_keywords = file_dir.split("/")
            new_keywords[len(new_keywords) - 1] = "new-keywords.txt"
            new_keywords = "/".join(new_keywords)
            new_keywords = str(new_keywords)
            file = open(new_keywords, "w")
            file.write(keywords)
            for i, j in redact_list:
                file.write(i + "," + j)
            file.close()
            new_nlp_keywords = redact(redact_list, nlp_keywords, nlp)
            current_window = Window2(total)
            current_window.start()
            obs = obfuscate_file(nlp, current_window, obs, new_nlp_keywords, doc)
            retreat = True
        if retreat:
            continue
        result = file_dir.split("/")
        result[len(result) - 1] = output_file_name + ".txt"
        result = "/".join(result)
        result = str(result)
        file = open(result, "w")
        for i in obs.lines:
            file.write(i)
        file.close()
    for i in obs.instances:
        model.add_training_data(i.subject, i.line)
    current_window = Window2(model.get_iterations())
    current_window.set_to_training()
    current_window.start()
    model.train(current_window)
    model.save()
    current_window = Window4(result, new_keywords, report_file)
    current_window.mainloop()
    break
